package main

import (
	"fmt"
	"net/http"
)

func route() {

	http.HandleFunc("/home", home)
	http.HandleFunc("/info", info)
	// add route to serve pictures
	http.Handle("/public/", http.StripPrefix("/public", http.FileServer(http.Dir("./public"))))
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	http.Handle("/favicon.ico", http.NotFoundHandler())

}

func home(w http.ResponseWriter, r *http.Request) {
	fmt.Println("starting home...")
	tpl.ExecuteTemplate(w, "home.html", nil)
}

func info(w http.ResponseWriter, r *http.Request) {
	fmt.Println("starting info...")
	tpl.ExecuteTemplate(w, "info.html", nil)
}
