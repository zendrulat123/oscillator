package main

import (
	"fmt"
	"html/template"
	"net/http"
)

func main() {

	//http.HandleFunc("/home", home)
	// add route to serve pictures
	//http.Handle("/public/", http.StripPrefix("/public", http.FileServer(http.Dir("./public"))))
	//http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	//http.Handle("/favicon.ico", http.NotFoundHandler())
	http.ListenAndServe(":8080", nil)

}

var tpl *template.Template

func init() {
	fmt.Println("starting...")
	tpl = template.Must(template.ParseGlob("template/*.html"))
}
